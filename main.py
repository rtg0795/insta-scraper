import tkinter as tk
from PIL import Image, ImageTk
import scrapper
from urllib.request import urlopen

class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.pack()
        self.create_widgets()

    def create_widgets(self):
        self.entry = tk.Entry(self)
        self.entry.grid(row=0, column=5)
        tk.Button(self, text='Search', command=self.crawl).grid(row=0, column=6)


    def process_result(self, result):
        if result.type == 'Image':
            content = Image.open(urlopen(result.content))
            content = content.resize((500, 500), Image.ANTIALIAS)
            ph = ImageTk.PhotoImage(content)
            self.img = tk.Label(self, width=450, height=450, image=ph)
            self.img.grid(row=2, columnspan=10)
            self.img.image = ph
        else:
            pass

    def crawl(self):
        try:
            self.img.pack_forget()
        except AttributeError:
            pass
        url = self.entry.get()
        result = scrapper.Scrapper(url)
        raw_image = self.process_result(result)
        self.process_result(result)
        self.display_time(result.time)
        self.display_type(result.type)

    def display_time(self, time_ms):
        self.time_label = tk.Label(self)
        self.time_label.grid(row=4, column=0)
        self.time_label.config(text=f'Scrapped in {time_ms} ms')

    def display_type(self, media_type):
        self.type_label = tk.Label(self)
        self.type_label.grid(row=4, column=9)
        self.type_label.config(text=f'Type: {media_type}')

root = tk.Tk()
root.geometry('500x500')
app = Application(master=root)
app.mainloop()