from bs4 import BeautifulSoup
import requests
import time


class Scrapper:
    def __init__(self, url):
        self.url = url
        self.time = 0
        self.content = ''
        self.type = ''
        begin = time.time()
        raw_source = requests.get(self.url).content
        self.soup = BeautifulSoup(raw_source, 'html.parser')
        self.find_type()
        end = time.time()
        self.time = round((end - begin) * 1000)

    def set_content(self, details):
        self.content = details['content']

    def find_type(self):
        video_details = self.soup.find('meta', property='og:video')
        image_details = self.soup.find('meta', property='og:image')
        if video_details:
            self.type = 'Video'
            self.set_content(video_details)
        else:
            self.type = 'Image'
            self.set_content(image_details)
